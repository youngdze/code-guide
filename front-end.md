# 前端编码规范

为了更好的编写和维护项目，我们需要遵循同一套编码规范，在此之前推荐一套较为成熟的编码规范——[Bootstrap编码规范](http://codeguide.bootcss.com/)，确保你已经详细阅读并了解了这些规范之后，这里重点强调一些项目进行中可能出现的细节问题

- [文件结构](#markdown-header-_1)
- [静态文件加载](#markdown-header-_2)
- [IE hack](#markdown-header-ie-hack)
- [自定义字体](#markdown-header-_3)

## 推荐的一种文件结构：

    css/*.css
    font/*
    img/*.(jpg|gif|png)
    js/*.js
    *.(html|php)

## 静态文件加载

先加载css文件再加载js文件，初始化的js代码需要放在head中，其他js文件，为了加快页面加载速度，可以考虑放在页面底部加载。另，强烈建议在css和主js文件后面加上版本号，格式为`?ver=更新日期`，如：

    <!DOCTYPE html>
    <html lang="en-US">
      <head>
        <title>Page title</title>
        <link rel="stylesheet" href="css/style.css?ver=20140526">
        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/jquery.plugins.js"></script>
        <script src="js/jquery.main.js?ver=20140526"></script>
      </head>
      <body>
        <!-- ... -->
      </body>
    </html>

## IE hack

推荐的两种解决方案（部分可能需要考虑到IE9的情况）：

方法一：

    <!DOCTYPE html>
    <!--[if IE 8]>
    <html class="ie ie8" lang="en-US">
    <![endif]-->
    <!--[if IE 9]>
    <html class="ie ie9" lang="en-US">
    <![endif]-->
    <!--[if !(IE 8) | !(IE 9)  ]><!-->
    <html lang="en-US">
    <!--<![endif]-->
    <head>

方法二：

    <!--[if lt IE 9]>
        <link rel="stylesheet" href="css/ie.css?ver=20140526">
    <![endif]-->
    
注，IE8不兼容HTML5中新增加的标签，需要加载html5.js来解决这个问题，需要注意的是html5.js必须加载在页面头部
    
    <!--[if lt IE 9]>
      <script src="js/html5.js"></script>
    <![endif]-->

## 自定义字体

推荐使用Google Fonts来解决自定义字体问题，你也可以把fonts的一系列文件放在fonts文件夹里，并在css文件里用`@font-face`声明，@font-face所需的.eot,.woff,.ttf,.svg字体格式，推荐一款字体生成工具[fontsquirrel](http://www.fontsquirrel.com/tools/webfont-generator)。另，安全字体代码可以通过[css font stack](http://cssfontstack.com/)获取。

方法一：

    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

方法二：

    @font-face {
      font-family: "Roboto";
      src: url("../fonts/Roboto.eot");
      src: url("../fonts/Roboto.eot?#iefix") format("embedded-opentype"),
      url("../fonts/Roboto#Roboto") format("svg"),
      url("../fonts/Roboto.woff") format("woff"),
      url("../fonts/Roboto.ttf") format("truetype");
      font-weight: 400;
      font-style: normal;
    }
